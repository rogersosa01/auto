let DataTableApp = class {
    constructor(url,
        div,
        columns,
        data={},
        arrayCondition=[]) {

        this.oTable; //atributo para la tabla
        this.myData=data; //Arreglo de datos
        this.div=div; //div donde se cargara la tabla 
        this.columns=columns; //columnas del datatable
        this.token=$('meta[name="csrf-token"]').attr('content');
        this.routeList=url; //ruta de datos de datatable
        this.arrayCondition=arrayCondition; //condicion de columnas que se van a mostrar
        this.render(); //formar datatable
    } 
    //formar datatable
    render(){
    	let self = this;
        this.oTable = $(this.div).DataTable({
            processing: 1,
            serverSide: 0,
            "pageLength": 10,
            responsive: 1,

            //responsive: 1,
            ajax: {
                url: this.routeList,
                headers: {
                    'X-CSRF-TOKEN': this.token
                },
                type: 'get',
    	        data:function ( d ) {
    	           return  $.extend(d, self.myData);
    	        }
            },
            columns: this.columns,
            "columnDefs": [
                {
                    "targets": this.arrayCondition,
                    "visible": false
                }, 
                {"className": "text-center", "targets":'_all'}
            ]
        });
    }
    //filtros de busquedas en las columnas
    filters(){
        $(this.div+' tfoot.filter tr td').each( function () {
            let title = $(this).text();
            if(!$(this).hasClass('no-filter'))
            $(this).html( '<input type="text" class="form-control" placeholder="Buscar '+title+'" />' );
        });

        let self=this;
        this.oTable.columns().every(function (index) {
            $(self.div+' tfoot tr td:eq(' + index + ') input').on('keyup change', function () {
                self.oTable.column($(this).parent().index() + ':visible')
                .search(this.value)
                .draw();
            });
        });
    }
    //filter por combos o personalizados
    filter(callback){
        callback();
    }
    //filtro de un combo por una columna especifica (Cuando el combo solo filtra por la data existente en el datatable, no va al servidor)
    filterColumn(div,column){
        self=this;
        $(div).change(function(event) {
            self.oTable.columns(column).search(this.value).draw();
        });
    }
    //evetnos sobre botones 
    eventDetail(div,callback,event='click'){
        let self=this;
        $(this.div).on(event, 'tbody '+div, function(event) {
            let data=self.oTable.row( $(this).parents('tr') ).data();
            if(data == null){
                data=self.oTable.row( $(this).parents('li') ).data();
            }
            callback(data,this,event);
        });
    }

}