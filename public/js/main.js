function notifyMessage(msj,title='',type,delay=5000){
    $.notify({
      message: msj,
      title: title
    },{
      type: type,
      delay: delay,
      z_index:1100
    });
}

function saveForm(form,data,dataType='json'){
    return new Promise((resolve,reject)=>{
        form.parsley().validate();
        let validate = form.parsley().isValid();
        if (validate) {
            $.ajax({ 
                url: form.attr('action'),
                type: form.attr('method'),
                dataType: dataType,
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function(data) {
                if(data.tipo=="aprobado"){
                    resolve(data);
                }else{
                    let menj='';
                    if(data.condicion){
                        menj='<ul>';
                        for (let i in JSON.parse(data.menj)) {
                            menj+=`<li>${JSON.parse(data.menj)[i]}</li>`;
                        }
                        menj+='</ul>';
                    }else{
                        menj=data.menj;
                    }
                    notifyMessage(menj,'Error:','danger');
                }
            })
            .fail(function(data) {
                notifyMessage('Ocurrió un error al procesar la solicitud, comuníquese con el administrador del sistema','Error al realizar el registro','danger');
            });
        }
    });
}

function  loadModal(url,idModal,data=[],callback=null,method='get'){
    $.ajax({
        url: url,
        type: method,
        dataType: 'html',
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    })
    .done( function(data) {
        $('#modal-load').html(data);
        $(idModal).modal('show');
        if(callback!=null){
            callback();
        }
    })
    .fail(function(data) {
        console.log(data);
    });
}

$('#modal-load').on('hidden.bs.modal', function (e) {
  $('#modal-load').html('');

})

$('#createModal').on('hidden.bs.modal', function (e) {
  $('#createModal').find('input,select').val('')
})

