<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serial');
            $table->string('description');
            $table->unsignedInteger('city_id');
            $table->timestamps();

            /*table*/
            $table->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealers', function (Blueprint $table) {
            $table->dropForeign('dealers_city_id_foreign');
        });
        Schema::dropIfExists('dealers');
    }
}