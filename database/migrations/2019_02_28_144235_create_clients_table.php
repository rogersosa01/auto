<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('dni')->unique();
            $table->string('email');
            $table->string('telephone_number');
            $table->unsignedInteger('locality_id');
            $table->unsignedInteger('dealer_id'); 
            $table->unsignedInteger('status_id'); 
            $table->timestamps();
            /**
                *Add Foreign
            */   
            $table->foreign('locality_id')
                ->references('id')
                ->on('localities')
                ->onUpdate('cascade'); 

            $table->foreign('dealer_id')
                ->references('id')
                ->on('dealers')
                ->onUpdate('cascade'); 

            $table->foreign('status_id')
                ->references('id')
                ->on('status')
                ->onUpdate('cascade'); 
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropForeign('clients_dealer_id_foreign');
            $table->dropForeign('clients_locality_id_foreign');
            $table->dropForeign('clients_status_id_foreign');
        });
        Schema::dropIfExists('clients');
        Schema::dropIfExists('status');
    }
}
