<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();

        $users = [
            [
                'name'       => 'admin admin',
                'email'=> 'admin@prueba.com',
                'password'=> bcrypt('123456')
            ], 

        ]; 

        foreach ($users as $user) {
            User::create($user);
        }


        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}