<?php

use Illuminate\Database\Seeder;
use App\Model\Dealer;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DealerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Dealer::truncate();

        factory(App\Model\Dealer::class,15)->create();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}