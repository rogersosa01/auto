<?php

use Illuminate\Database\Seeder;
use App\Model\Status;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Status::truncate();

        $status = [
            [
                'description'       => 'Activo',
            ],            
            [
                'description'       => 'Inactivo',
            ],   

        ]; 

        foreach ($status as $value) {
            Status::create($value);
        }


        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}