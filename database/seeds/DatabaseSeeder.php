<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CitySeeder::class);
        $this->call(LocalitySeeder::class);
        $this->call(DealerSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(UserSeeder::class);
    }
}
