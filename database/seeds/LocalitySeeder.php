<?php

use Illuminate\Database\Seeder;
use App\Model\Locality;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LocalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Locality::truncate();

        $localities = [
            [
                'description'       => 'Localidad 1',
            ],            
            [
                'description'       => 'Localidad 2',
            ],            
            [
                'description'       => 'Localidad 3',
            ],            
            [
                'description'       => 'Localidad 4',
            ],           
            [
                'description'       => 'Localidad 5',
            ],   

        ]; 

        foreach ($localities as $locality) {
            Locality::create($locality);
        }


        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}