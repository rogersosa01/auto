<?php

use Illuminate\Database\Seeder;
use App\Model\City;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        City::truncate();

        $cities = [
            [
                'description'       => 'Caracas',
            ],            
            [
                'description'       => 'Valencia',
            ],            
            [
                'description'       => 'Maracay',
            ],            
            [
                'description'       => 'Barquisimeto',
            ],           
            [
                'description'       => 'Maracaibo',
            ],   

        ]; 

        foreach ($cities as $city) {
            City::create($city);
        }


        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}