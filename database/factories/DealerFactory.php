<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
use App\Model\Dealer;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Dealer::class, function (Faker $faker) use ($factory) {
	$serial=str_random(2);
    return [
    	'serial' => 'CONC'.$serial,
    	'city_id' => App\Model\City::all()->random()->id,
	    'description' => 'concesionario '.$serial,
	    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
	    'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
    ];
});
