<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
	Route::get('clients/datatable','ClientController@datatable')->name('clients.datatable');
	Route::get('clients/report/pdf','ClientController@reportPdf')->name('clients.report.pdf');
	Route::get('clients/report','ClientController@viewReport')->name('clients.report');
	Route::resource('clients', 'ClientController');
	Route::get('/', 'HomeController@index')->name('home');
});
Auth::routes();

