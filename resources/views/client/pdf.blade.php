<html>
<head>
  <style>
    @page { margin: 100px 25px; }
    header { position: fixed; top: -60px; left: 0px; right: 0px; height: 50px; }
    footer { position: fixed; bottom: -60px; left: 0px; right: 0px; height: 50px; }
    footer .pagenum:before {
          content: counter(page);
    } 
    /*.page-b { page-break-after: always; }
    .page-b:last-child { page-break-after: never; }*/

    body, html {
             height: 100vh;
            }

            body { 
                /* The image used */
                /* Full height */
                height: 100vh; 

                /* Center and scale the image nicely */
                background-position: center top;
                background-repeat: no-repeat;
                background-size: cover;
                margin: -100px -25px;
                padding: 10% 5%;
            }
              .date{
                 padding-left: 75%;
            }

             /* HTML5 display-role reset for older browsers */
            article, aside, details, figcaption, figure, 
            footer, header, hgroup, menu, nav, section {
                display: block;
            }
            body {
                line-height: 1;
            }
            ol, ul {
                list-style: none;
            }
            blockquote, q {
                quotes: none;
            }
            blockquote:before, blockquote:after,
            q:before, q:after {
                content: '';
                content: none;
            }
            table {
                border-collapse: collapse;
                border-spacing: 0;
                background-size: cover;
            }
            table.no-spacing {
              border-spacing:0; /* Removes the cell spacing via CSS */
              border-collapse: collapse;  /* Optional - if you don't want to have double border where cells touch */
            }
            .img-logo{
                width: 200px;
                height: 150px;
            }
            .inline{
                display: inline-block;
            }
            .tg  {border-collapse:collapse;border-spacing:0; text-align: center; margin-left: 10px;}
            .tg td{font-family:Arial, sans-serif;font-size:20px;padding:10px 20px;overflow:hidden;word-break:normal;}
            .tg th{font-family:Arial, sans-serif;font-size:20px;font-weight:normal;padding:10px 5px;overflow:hidden;word-break:normal; }
            .tg .tg-yw4l{vertical-align:top; background-color: #0275db;font-size:14px}
            .report-title {
                font-size: 15px;
            }
            .ml-2 {
                margin-left: 1em;
            }
            .mr-5 {
                margin-right: 2em;
           }
           .title{
            font-family:Arial, sans-serif;font-size: 25px;
            margin-top: 0px;
            text-align: center;
           }

           .description{
            text-align: left;
            margin-bottom: 10px;
            font-family: Arial;
           }
           .description p{
            margin: 0;
            font-size: 14px;
           }

           table.report {
              width: 100%;
              text-align: center;
              border-collapse: collapse;
              font-family: Arial;
              margin-bottom: 100px;
            }
            table.report td, table.report th {
              padding: 4px 10px;
            }
            table.report tbody td {
              font-size: 15px;
            }
            table.report tr:nth-child(even) {
              background: #C4C4C4;
            }
            table.report thead {
              background: #2E2E2E;
              border-bottom: 3px solid #0275db;
            }
            table.report thead th {
              font-size: 15px;
              font-weight: bold;
              color: #FFFFFF;
              text-align: center;
              padding: 10px;
            }
            table.report tfoot td {
              font-size: 13px;
            }
            table.report tfoot .links {
              text-align: right;
            }
            table.report tfoot .links a{
              display: inline-block;
              background: #FFFFFF;
              color: #A40808;
              padding: 2px 8px;
              border-radius: 5px;
            }
            .mt{
                margin-top: 20px;
            }
            #header { position: fixed; left: 0px; top: -180px; right: 0px; height: 150px; background-color: #0275db; text-align: center; }
            .spacing-table{
                margin-top: 500px;
            }
  </style>
</head>
<body>
  <header>
     <div class="date">{{Carbon\Carbon::now()}}</div>
  </header>
  <footer style="text-align: center;"><div class="pagenum-container"> Página <span class="pagenum"> </span></div></footer>
  <main>
    <div class="inline mr-5 ">    
    </div>
    <div class="inline report-title ml-2">
        <table class="tg">
            <tr>
                <th class="tg-031e">
                 <h1 style=" margin: 0; padding:0 ;text-align: center;">Automotriz</h1>
                </th>
            </tr>
            <tr>
                <td class="tg-yw4l">
                 <h2 style=" margin: 0; padding:0 ;text-align: center; color: white;">
                 Reporte de Clientes
                 </h2>    
                 </td>
            </tr>
            <tr><td></td></tr>
             <tr><td></td></tr>
              <tr><td></td></tr>
        </table>
    </div>
    <h1 class="title">Listado de clientes</h1>
   
    <br>
    <p >
        <table class="page-b report">
            <thead>
                <tr>
                   	<th>Cédula</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Teléfono</th>
                    <th>Localidad</th>
                    <th>Concesionario</th>
                </tr>
            </thead>
            <tbody>
            @foreach($data as $value)	
                <tr>
                    <td>{{$value->dni}}</td>
                    <td>{{$value->first_name}} {{$value->last_name}}</td>
                    <td>{{$value->email}}</td>
                    <td>{{$value->telephone_number}}</td>
                    <td>{{$value->locality->description}}</td>
                    <td>{{$value->dealer->description}}</td>
                </tr>
            @endforeach
           </tbody>
        </table>
    </p>
  </main>
</body>
</html>