@extends('home')
@section('content')
    <div class="card">
      <div class="card-header">
        Listado de Clientes
      </div>
      <div class="card-body ">
        <div class="row p-3 justify-content-between">
            <div class="col-4">
                <label>Concesionario:</label>
                <select   id="dealer_id" class="form-control" required="">  
                    <option value="">Seleccione</option>
                    @foreach($dealers as $dealer) 
                    <option  value="{{ $dealer->id }}">{{ $dealer->description }}</option>
                    @endforeach
                </select>       
            </div>
            <div class="col-3">
                <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#createModal">
                  Registrar Cliente
                </button>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-12">
                <table id="clientsGrilla" class="mt-5 table table-borderless table-striped table-earning w-100" cellspacing="0">
                    <thead>
                        <tr>
                            <th data-priority="0">Nombre</th>
                            <th>Cédula</th>
                            <th>Teléfono</th>
                            <th>Concesionario</th>
                            <th data-priority="1">Acción</th>
                        </tr>
                    </thead>
                    <tfoot class="filter">
                        <tr>
                            <td>Nombre</td>
                            <td>Cédula</td>
                            <td>Teléfono</td>
                            <td>Concesionario</td>
                            <td class="no-filter"></td>
                        </tr>
                    </tfoot>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
      </div>
    </div>
@endsection

<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Nuevo Cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

      </div>
      <form action="{{route('clients.store')}}" method="post" id="createForm" data-parsley-validate='' novalidate='' data-parsley-excluded='[disabled]'data-parsley-focus='first'>
          <div class="modal-body">
                <label for="dni">Cédula</label>
              <div class="form-group row">
                <div class="col-2">
                    <select name="letter" class="form-control" required="">
                        <option value="V"> V</option>
                        <option value="E"> E</option>
                    </select>
                </div>
                <div class="col-10">
                    <input type="text" maxlength="8" class="form-control dni" name="dni" id="dni" placeholder="Cédula" required>
                </div>
              </div>
              <div class="form-group">
                <label for="first_name">Nombres</label>
                <input type="text" class="form-control" maxlength="100" name="first_name" id="first_name" placeholder="Nombres" required>
              </div>
              <div class="form-group">
                <label for="last_name">Apellidos</label>
                <input type="text" class="form-control" maxlength="100" name="last_name" id="last_name" placeholder="Apellidos" required>
              </div>                  
              <div class="form-group">
                <label for="email">Correo</label>
                <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Correo" required>
              </div>
              <div class="form-group">
                <label for="telephone_number">Teléfono</label>
                <input type="text" class="form-control phone" name="telephone_number" id="telephone_number" placeholder="(0212)-555.55.55" required>
              </div>  
                <div class="form-group">
                    <label>Localidad:</label>
                    <select  name="locality_id" class="form-control" required="">  
                        <option>Seleccione</option>
                        @foreach($locations as $location) 
                        <option value="{{ $location->id }}">{{ $location->description }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Concesionario:</label>
                    <select  name="dealer_id" class="form-control" required="">  
                        <option>Seleccione</option>
                        @foreach($dealers as $dealer) 
                        <option  value="{{ $dealer->id }}">{{ $dealer->description }}</option>
                        @endforeach
                    </select>
                </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="subtmi" class="btn btn-success">Guardar</button>
          </div>
      </form>
    </div>
  </div>
</div>

@section('javascript')
    <script>
        
        (()=>{
            class App{
                constructor(){
                    this.eventCreate()
                    this.routeDatatable="{{route('clients.datatable')}}";
                    this.routeModal="{{route('clients.edit',":slug")}}";
                    this.routeModalView="{{route('clients.show',":slug")}}";
                    this.routeModalDelete="{{route('clients.destroy',":slug")}}";
                    this.divDatatable='#clientsGrilla';
                    this.ColumnsDatatable=[
                        {data: 'full_name'},
                        {data: 'dni'},
                        {data: 'telephone_number'},
                        {data: 'dealer'},
                        {data: 'actions'},
                    ];
                    this.dataDatatable;
                    this.table;
                    this.arrayCondition;
                    this.showDataTable()
                }
                eventCreate(){
                    let self=this;
                    $('#createModal').on("submit","#createForm",function(event){
                        event.preventDefault()
                        let form = $(this);
                        let data=form.serialize();
                        saveForm(form,data)
                        .then(
                            (data)=>{
                                $('#createModal').modal('hide');
                                notifyMessage(data.menj,'Operación Exitosa.','success');
                                self.table.oTable.ajax.reload();
                            },
                            (error)=>{
                                if(error.error==1){
                                    notifyMessage('Debe seleccionar una respuesta correcta para el tipo de pregunta seleccionada.','Error de datos','info');
                                }else if(error.error==2){
                                    notifyMessage('Debe existir más de una respuesta.','Error de datos','info');
                                }
                            }
                        )
                    })
                }
                maskPhone(div){
                    div.mask('(0000) 000-00.00')
                }
                maskNumber(div){
                    div.mask('##');
                }
                showDataTable(){
                    //***Crear DataTable***//
                    this.table=new DataTableApp(this.routeDatatable,this.divDatatable,this.ColumnsDatatable,this.dataDatatable,this.arrayCondition);
                    //*** Filtros individuales para cada columna ***//
                    this.table.filters();

                     //***Evento de editar cliente ***//
                    this.table.eventDetail('#edit',(data)=>{
                        let self=this;
                        // se obtiene el id del registro para enviarlo al formulario
                        let idClient=data.id;
                        let url = this.routeModal.replace(':slug', idClient);

                        loadModal(url,'#modalEditClient',data,function(){
                            $('#modalEditClient').on("submit","#procesarForm",function(event){
                                event.preventDefault();
                                let form = $(this);
                                let data=form.serialize()+"&id="+idClient;
                                saveForm(form,data)
                                .then((data)=>{
                                    $('#modalEditClient').modal('hide');
                                    notifyMessage(data.menj,'Registro exitoso','success');
                                    self.table.oTable.ajax.reload();
                                })

                            });    
                        });
                   });
                    this.table.filter(()=>{
                        $('#dealer_id').change((event)=> {
                            this.table.myData.dealer_id = $('#dealer_id').val();
                            this.table.oTable.ajax.url(this.routeDatatable).load();
                        });
                    })
                    this.table.filter(()=>{
                        $('#locality_id').change((event)=> {
                            this.table.myData.locality_id = $('#locality_id').val();
                            this.table.oTable.ajax.url(this.routeDatatable).load();
                        });
                    })
                    //*
                    //***Evento de agregar cliente ***//
                    this.table.eventDetail('#view',(data)=>{
                        let self=this;
                        // se obtiene el id del registro para enviarlo al formulario
                        let idClient=data.id;
                        let url = this.routeModalView.replace(':slug', idClient);
                        // se levanta la modal y se le envia la data
                        loadModal(url,'#modalViewClient',data);
                   });

                    //***Evento de eliminar participante ***//
                    this.table.eventDetail('#remove',(data,obj)=>{
                        let self=this;
                        // se obtiene el id del registro para enviarlo al formulario
                        let idClient=data.id;
                        bootbox.confirm({
                            title: "Confirmar Acción?",
                            message: `¿Está seguro que desea eliminar al cliente ${data.full_name}`,
                            buttons: {
                                cancel: {
                                    label: '<i class="fa fa-times"></i> Cancel'
                                },
                                confirm: {
                                    label: '<i class="fa fa-check"></i> Confirm'
                                }
                            },
                            callback: function (result) {
                                if(result){
                                    let url = self.routeModalDelete.replace(':slug', idClient);
                                    $.ajax({
                                        type: 'DELETE',
                                        url: url,
                                        dataType: 'json',//
                                        headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                        data: data,
                                    })
                                    .done(function(data) {
                                        if(data.tipo=="aprobado"){
                                            notifyMessage(data.menj,'','success');
                                            self.table.oTable.ajax.reload(); //recargar data de la tabla
                                        }else{
                                            notifyMessage(data.menj,'Error al realizar la modificación','danger');
                                        }
                                    })
                                    .fail(function(data) {
                                        notifyMessage('Ocurrió un error al procesar su formulario, comuníquese con el administrador del sistema','Error al realizar el registro','danger');
                                    });
                                }
                            }
                        });
                    });
                }
            }
            let app=new App();
            app.maskPhone($('.phone'));
            app.maskNumber($('.dni'));
        })();
    </script>
@stop