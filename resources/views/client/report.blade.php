@extends('home')
@section('content')
    <div class="card">
      <div class="card-header">
        Reporte de clientes
      </div>
      <div class="card-body ">
      	<form method="get" action="{{route('clients.report.pdf')}}">
	      	<div class="form-group row">
	      		<div class="col-6">
	      			<label>Concesionario:</label>
		            <select   name="dealer_id" class="form-control" >  
		                <option value="">Seleccione</option>
		                @foreach($dealers as $dealer) 
		                <option  value="{{ $dealer->id }}">{{ $dealer->description }}</option>
		                @endforeach
		            </select>
	      		</div>
	      		<div class="col-6">
		            <label>Localidad:</label>
		            <select  name="locality_id" class="form-control" >  
		                <option value="">Seleccione</option>
		                @foreach($locations as $location) 
		                <option value="{{ $location->id }}">{{ $location->description }}</option>
		                @endforeach
		            </select>
	      		</div>
	        </div>	
	        <div class="form-group row justify-content-center" >
	        	<div class="col-2">
	        		<button type="submit" class="btn btn-danger">
		              <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Descargar
		            </button>
	        	</div>
	        </div>
        </form>
      </div>
    </div>
@endsection
