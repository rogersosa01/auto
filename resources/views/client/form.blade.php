<div class="row justify-content-center">
	<div class="col-md-12 col-md-auto">
        <div class="form-group">
			<label>Cédula:</label>
			<input type="text" class="form-control" value="{{ $request->dni }}" name="dni" readonly="" >
		</div>
    </div>
	<div class="col-md-12 col-md-auto">
        <div class="form-group">
			<label>Nombres:</label>
			<input type="text" class="form-control" maxlength="100" value="{{ $request->first_name }}" name="first_name" @if(isset($exist)) readonly="" @endif>
		</div>
    </div>
    <div class="col-md-12 col-md-auto">
        <div class="form-group">
			<label>Apellidos:</label>
			<input type="text" class="form-control" maxlength="100" value="{{ $request->last_name }}" name="last_name" @if(isset($exist)) readonly="" @endif>
		</div>
    </div>
    <div class="col-md-12 col-md-auto">
    <div class="form-group">
	    <label for="email">Correo</label>
	    <input type="email" class="form-control" id="email" name="email" value="{{ $request->email }}" aria-describedby="emailHelp" @if(isset($exist)) readonly="" @endif placeholder="Correo" required>
	  </div>
	</div>
    <div class="col-md-12 col-md-auto">
        <div class="form-group">
			<label>Teléfono:</label>
			<input type="text" class="form-control phone" value="{{ $request->telephone_number }}" name="telephone_number" @if(isset($exist)) readonly="" @endif>
		</div>
    </div>
	<div class="col-md-12 col-md-auto">
	    <div class="form-group">
			<label>Localidad:</label>
			<select  name="locality_id" class="form-control" required="" @if(isset($exist)) readonly="" @endif>  
				<option>Seleccione</option>
				@foreach($locations as $location) 
				<option @if($location->id == $request->locality_id) selected="" @endif value="{{ $location->id }}">{{ $location->description }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-md-12 col-md-auto">
	    <div class="form-group">
			<label>Concesionario:</label>
			<select  name="dealer_id" class="form-control" required="" @if(isset($exist)) readonly="" @endif>  
				<option>Seleccione</option>
				@foreach($dealers as $dealer) 
				<option @if($dealer->id == $request->dealer_id) selected="" @endif value="{{ $dealer->id }}">{{ $dealer->description }}</option>
				@endforeach
			</select>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.phone').mask('(0000) 000-00.00');
	$('.dni').mask('V00**');
</script>