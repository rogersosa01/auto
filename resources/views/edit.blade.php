<div class="modal fade" id="modalEditClient" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<form action="{{route('clients.update',$request->id)}}" method="PUT" id="procesarForm" data-parsley-validate='' novalidate='' data-parsley-excluded='[disabled]'data-parsley-focus='first'>
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="largeModalLabel">Editar Clientes</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				@include('form')				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
		</div>
		</form>
	</div>
</div>
