<div class="logo">
    <h2>AutoMotriz</h2>
</div>
<nav class="menu d-flex d-sm-block justify-content-center flex-wrap">
    <a href="{{route('clients.index')}}"><span>Clientes</span></a>
    <a href="{{route('clients.report')}}"><span>Reporte</span></a>
    <a href="{{ route('logout') }}"
       onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
        </i><span>Cerrar Sesión</span>
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</nav>