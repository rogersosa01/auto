<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Automotriz</title>

        <!-- Fonts -->
        <link href="{!! asset('css/font-awesome-4.7/css/font-awesome.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
        <link href="{!! asset('css/bootstrap.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
        <link href="{!! asset('css/datatable/datatable.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
        <link href="{!! asset('css/estilos.css') !!}" media="all" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="modal-load">
            @yield('modal')
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="barra-lateral col-12 col-sm-auto">
                    @include('menu')
                </div>

                <main class="main col">
                    <div class="row justify-content-center">
                        <div class="col-10">
                            @yield('content')
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <script type="text/javascript" src="{!! asset('js/jquery-3.2.1.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/bootstrap-4.1/popper.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/bootstrap-4.1/bootstrap.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/parsley.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/bootstrap-notify.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/datatable/datatable.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/mask.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/bootbox.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/main.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/class/Datatable.js') !!}"></script>
        @yield('javascript')
    </body>
</html>
