<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Client;
use App\Model\Dealer;
use App\Http\Requests\StoreClient;
use App\Http\Requests\UpdateClient;
use App\Model\Locality;
use DataTables;
use DB;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Routing\Redirect;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dealers = Dealer::all();
        $locations = Locality::all();
        return view('client.index',compact('dealers','locations'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewReport()
    {
        $dealers = Dealer::all();
        $locations = Locality::all();
        return view('client.report',compact('dealers','locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClient $request)
    {
        DB::beginTransaction();
        try { 
            $client = Client::updateOrCreate(['dni'=>$request->letter.$request->dni],[
                                'first_name' => $request->first_name,
                                'last_name' => $request->last_name,
                                'telephone_number' => $request->telephone_number,
                                'email' => $request->email,
                                'dealer_id' => $request->dealer_id,
                                'locality_id' => $request->locality_id,
                                'status_id'=>1
                            ]);

            DB::commit(); 
            return response()->json(array('menj'=>'Operación completada correctamente','tipo'=>'aprobado'));
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(array('menj'=>'Ocurrió un error al registrar, comuníquese con el administrador del sistema','tipo'=>'rechazado'));
        } catch(\PDOException $e) {
            DB::rollBack();
            return response()->json(array('menj'=>'Ocurrió un error al registrar, comuníquese con el administrador del sistema','tipo'=>'rechazado'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request){
        $dealers = Dealer::all();
        $locations = Locality::all();
        $exist = true;
        return view('client.view',compact('dealers','locations','request','exist'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($value, Request $request)
    {
        $dealers = Dealer::all();
        $locations = Locality::all();
 
        return view('client.edit',compact('dealers','locations','request'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateClient $request)
    {
        DB::beginTransaction();
        try { 
            $updateClient = Client::where('id', $id)
                            ->update([
                                'first_name' => $request->first_name,
                                'last_name' => $request->last_name,
                                'dni' => $request->dni,
                                'email' => $request->email,
                                'telephone_number' => $request->telephone_number,
                                'dealer_id' => $request->dealer_id,
                                'locality_id' => $request->locality_id,
                            ]);

            DB::commit(); 
            return response()->json(array('menj'=>'Operación completada correctamente','tipo'=>'aprobado'));
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(array('menj'=>'Ocurrió un error al registrar, comuníquese con el administrador del sistema','tipo'=>'rechazado'));
        } catch(\PDOException $e) {
            DB::rollBack();
            return response()->json(array('menj'=>'Ocurrió un error al registrar, comuníquese con el administrador del sistema','tipo'=>'rechazado'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){

        DB::beginTransaction();
        try { 
            $updateClient = Client::where('id', $id)
                            ->update([
                                'status_id' => 2,
                            ]);
            DB::commit(); 
            return response()->json(array('menj'=>'Se elimino correctamente el usuario','tipo'=>'aprobado'));
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(array('menj'=>'Ocurrió un error al registrar, comuníquese con el administrador del sistema','tipo'=>'rechazado'));
        } catch(\PDOException $e) {
            DB::rollBack();
            return response()->json(array('menj'=>'Ocurrió un error al registrar, comuníquese con el administrador del sistema','tipo'=>'rechazado'));
        }

    }

    public function datatable(Request $request)
    {
        //se obtienen los clientes que se encuentren activo
        $list = Client::where('status_id',1);

        if(isset($request->dealer_id)){
            $list=$list->where('dealer_id',$request->dealer_id);
        }

        if(isset($request->locality_id)){
            $list=$list->where('locality_id',$request->locality_id);
        }

        $data =DataTables::collection($list->get())
        ->addColumn('full_name',function($row){ // nombres del cliente
            return $row->first_name.' '.$row->last_name; 
        })
       ->addColumn('dealer',function($row){
            return $row->dealer->description;  //se obtiene el consecionario del cliente
        })
       ->addColumn('actions',function($row) { //las acciones de editar, ver y eliminar del cliente
            return '<span style="color: green; cursor:pointer" title="Ver" id="view" class="fa fa-eye  fa-lg"></span>
                    <span style="color: #0b57d9; cursor:pointer" title="Editar" id="edit" class="fa fa-edit fa-lg"></span>
                    <span style="color: red; cursor:pointer" title="Eliminar" id="remove" class="fa fa-remove fa-lg"></span>';
        })->rawColumns(['actions'])
        ->make(true);
        return $data;
    }

    public function reportPdf(Request $request){
        $list = Client::where('status_id',1);

        if(isset($request->dealer_id)){
            $list=$list->where('dealer_id',$request->dealer_id);
        }

        if(isset($request->locality_id)){
            $list=$list->where('locality_id',$request->locality_id);
        }
        $list=$list->get();
        if($list->count()>0){        
            $pdf = PDF::loadView('client.pdf',['data'=>$list]);
            $pdf->setPaper('A4');
            return $pdf->download('report.pdf');
        }

        return redirect()->route('clients.report');
    }
}
