<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
  protected $fillable = ['description'];
}
