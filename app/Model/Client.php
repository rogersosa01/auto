<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Client extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
	
  	protected $fillable = [
	  	'first_name', 
	  	'last_name',
	  	'dni',
	  	'telephone_number',
	  	'locality_id',
	  	'email',
	  	'dealer_id',
	  	'status_id'
	];

	public function generateTags(): array
    {
        return [
            $this->table
        ];
    }

	public function locality()
    {
        return $this->belongsTo(Locality::class);
    } 

    public function dealer()
    {
        return $this->belongsTo(Dealer::class);
    } 
}
