<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Dealer extends Model
{
	protected $fillable = [
	  	'serial', 
	  	'description',
	  	'city_id'
	];
	public function city()
    {
        return $this->belongsTo(City::class);
    }	

    public function clients()
    {
        return $this->hasMany(Client::class);
    }
}
